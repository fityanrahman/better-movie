# Better Movies (Jetpack Compose)

Movies App using [The Movie Database](https://www.themoviedb.org) built with jetpack compose.

<p float="left">
  <img width="30%" height="50%" src="https://gitlab.com/fityanrahman/better-movie/-/raw/main/screenshots/Screenshot_home.jpg" />
  <img width="30%" height="50%" src="https://gitlab.com/fityanrahman/better-movie/-/raw/main/screenshots/Screenshot_genre.jpg" />
  <img width="30%" height="50%" src="https://gitlab.com/fityanrahman/better-movie/-/raw/main/screenshots/Screenshot_detail.jpg" />
</p>

# Features
- Movie list
- Movie list by genre
- Movie detail
- Movie cast
- Movie review
- Movie trailer