package id.frame.bettermovies.data.remote.video

data class VideoResponse(
    val id: Int,
    val results: List<Video>
)