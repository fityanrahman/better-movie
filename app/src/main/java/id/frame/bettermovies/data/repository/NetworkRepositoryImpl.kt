package id.frame.bettermovies.data.repository

import id.frame.bettermovies.common.Resource
import id.frame.bettermovies.data.remote.ApiService
import id.frame.bettermovies.data.remote.credits.CreditsResponse
import id.frame.bettermovies.data.remote.genre.GenreResponse
import id.frame.bettermovies.data.remote.movie.MovieResponse
import id.frame.bettermovies.data.remote.movie_detail.toMovieDetail
import id.frame.bettermovies.data.remote.reviews.ReviewsResponse
import id.frame.bettermovies.data.remote.video.VideoResponse
import id.frame.bettermovies.domain.model.HomeList
import id.frame.bettermovies.domain.model.MovieDetail
import id.frame.bettermovies.domain.repository.NetworkRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class NetworkRepositoryImpl @Inject constructor(
    private val api: ApiService,
) : NetworkRepository {

    override suspend fun getGenres(): GenreResponse {
        return api.getGenres()
    }

    override suspend fun getPopularMovies(page: Int): MovieResponse {
        return api.getPopularMovies(page = page)
    }

    override suspend fun getMovieWithGenres(page: Int, genreId: Int): MovieResponse {
        return api.getMovieWithGenres(page = page, genreId = genreId)
    }

    override suspend fun getVideo(id: Int): VideoResponse {
        return api.getVideo(movieId = id)
    }

    override suspend fun getReviews(id: Int, page: Int): ReviewsResponse {
        return api.getReviews(movieId = id, page = page)
    }

    override suspend fun getCredits(id: Int): CreditsResponse {
        return api.getCredits(movieId = id)
    }

    override fun getMovieById(id: Int): Flow<Resource<MovieDetail>> = flow {
        emit(Resource.Loading())
        try {
            val movie = api.getMovieDetail(movieId = id).toMovieDetail()
            emit(Resource.Success(movie))
        } catch (e: Exception) {
            emit(Resource.Error(e.localizedMessage ?: "Error"))
        }

    }

    override fun getHomeMovies(): Flow<Resource<List<HomeList>>> = flow {
        emit(Resource.Loading())
        try {
            val genres = api.getGenres().genres
            val popular = api.getPopularMovies(page = 1).results

            val list = listOf(HomeList.Genres(genres), HomeList.Popular(popular))

            emit(Resource.Success(list))
        } catch (e: Exception) {
            emit(Resource.Error(e.localizedMessage ?: "An uncexpected error occured"))
        }
    }
}