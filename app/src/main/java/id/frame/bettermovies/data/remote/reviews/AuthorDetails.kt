package id.frame.bettermovies.data.remote.reviews

data class AuthorDetails(
    val avatar_path: String,
    val name: String,
    val rating: Int,
    val username: String
)