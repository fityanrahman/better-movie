package id.frame.bettermovies.domain.repository

import id.frame.bettermovies.common.Resource
import id.frame.bettermovies.data.remote.credits.CreditsResponse
import id.frame.bettermovies.data.remote.genre.GenreResponse
import id.frame.bettermovies.data.remote.movie.MovieResponse
import id.frame.bettermovies.data.remote.reviews.ReviewsResponse
import id.frame.bettermovies.data.remote.video.VideoResponse
import id.frame.bettermovies.domain.model.HomeList
import id.frame.bettermovies.domain.model.MovieDetail
import kotlinx.coroutines.flow.Flow

interface NetworkRepository {

    suspend fun getGenres(): GenreResponse

    suspend fun getPopularMovies(page: Int): MovieResponse

    suspend fun getMovieWithGenres(page: Int, genreId: Int): MovieResponse

    fun getMovieById(id: Int): Flow<Resource<MovieDetail>>

    suspend fun getVideo(id: Int): VideoResponse

    suspend fun getReviews(id: Int, page: Int): ReviewsResponse

    suspend fun getCredits(id: Int): CreditsResponse

    fun getHomeMovies(): Flow<Resource<List<HomeList>>>

}