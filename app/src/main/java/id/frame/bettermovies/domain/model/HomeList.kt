package id.frame.bettermovies.domain.model

import id.frame.bettermovies.data.remote.genre.Genre
import id.frame.bettermovies.data.remote.movie.Movie

sealed class HomeList {
    data class Genres(val genres: List<Genre>) : HomeList()
    data class Popular(val popular: List<Movie>) : HomeList()
}