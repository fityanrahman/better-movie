package id.frame.bettermovies.ui.screen.home

import id.frame.bettermovies.domain.model.HomeList

data class HomeState (
    val homeList: List<HomeList> = emptyList(),
    val isLoading: Boolean = false,
    val error: String = ""
)